import React from "react";
import ReactDOM  from "react-dom";
import MainPage from "./components/main-page/main-page";

import "./style/main_style.css";

ReactDOM.render(<MainPage/>, document.querySelector('.main-page'));