import React from "react";
import Table from "../table/table";
import Title from "../title/title";
import Paragraph from "../paragraph/paragraph";

function Main (){
    return(
        <main className="main">
            <section className="homework-1">
                <Title text={"Завдання 1."}/>
                <Paragraph text={"Створити таблицю зі Знаками зодіаку за допомогою 'React' та вивести на сторінку."}/>
                <div>
                    <Table/>
                </div>
                <Title text={"Завдання 2."}/>
                <Paragraph text={"Створити вертикальну панель навігації за допомогою 'React' та вивести на сторінку. Принаведенні - відкривається праворуч. Перемикач 'світла/темна' - працює як чекбокс"}/>
            </section>
        </main>
    )
}

export default Main;