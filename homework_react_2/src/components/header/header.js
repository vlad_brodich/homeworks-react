import React from "react";

function Header() {
    return(
        <header className="header">
            <nav  className="header-nav">
                <h1>Домашня робота з 'React' №2.</h1>
            </nav>
        </header>
    )
}

export default Header;