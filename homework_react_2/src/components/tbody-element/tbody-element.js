import React from "react";

function TbodyElement({num,icon,name,text}){
    return(
        <tr>
        <td>{num}</td>
        <td>{icon}</td>
        <td>{name}</td>
        <td>{text}</td>
    </tr>
    )
}

export default TbodyElement;