import React from "react";
import Icon from "../icon/icon";
import Link from "../link/link";

function BoxItem({className,src,alt,text}){
    return(
      <div className="nav-box-item">
        <div className="nav-box-item-img">
          <Icon src={src} alt={alt}></Icon>
        </div>
        <Link className={className} textLink={text}/>
      </div>
    )
}

export default BoxItem;