import React from "react";
import Paragraph from "../paragraph/paragraph";

function Footer(){
    return(
        <footer className="footer">
            <div className="footer-box">
                <Paragraph text={"29 Квітня 2023 р."}/>
            </div>
      </footer>
    )
}

export default Footer;