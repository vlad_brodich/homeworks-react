import React from "react";

const Link = ({className="",href="/#",textLink=""})=>{
    return(
        <a className={className} href={href}>{textLink}</a>
    )
}

export default Link