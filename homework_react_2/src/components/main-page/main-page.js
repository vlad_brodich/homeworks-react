import React from "react";
import Header from "../header/header";
import Main from "../main/main";
import Footer from "../footer/footer";
import Aside from "../aside/aside";


function MainPage() {
    return (
        <>
            <Header></Header>
            <Main></Main>
            <Footer></Footer>
            <Aside></Aside>
        </>
    )
}

export default MainPage;