import React from "react";

import  Icon  from "../icon/icon";
import BoxItem from "../box-item/box-item";
import house from "../../img/svg/house.svg";
import search from "../../img/svg/search.svg";
import Link from "../link/link";
import Title from "../title/title";
import Paragraph from "../paragraph/paragraph";


import revenue from "../../img/svg/revenue.svg";
import bell from "../../img/svg/bell.svg";
import analytics from "../../img/svg/analytics.svg";
import inventory from "../../img/svg/inventory.svg";
import exit from "../../img/svg/exit.svg";
import sunLength from "../../img/svg/sun-light.svg";
import sunYellow from "../../img/svg/sun-yellow.svg";
import moon from "../../img/svg/moon.svg";


function Aside(){
    return(
      <aside className="side-panel-nav">
        <div className="side-panel-nav-box">
          <div className="side-panel-nav-btn"></div>

          <div className="side-header">
            <div className="side-header-logo">VB</div>
            <div className="hide side-header-info">
              <Title text={"VladBrodich"}/>
              <Link className="hide" href="mailto:brodich_vlad@ukr.net" textLink='brodich_vlad@ukr.net'/>
            </div>
          </div>

          <div className="side-search-box">
            <div className="search-icon">
            <Icon src={search} alt={'search-icon'}></Icon>
            </div>
            <input className="hide input-search" type="search" placeholder="Search..."/>
          </div>

          <nav className="side-nav-box">
            <BoxItem className={"hide"} src={house} alt={'house-icon'} text= {"Dashboard"}></BoxItem>
            <BoxItem className={"hide"}  src={revenue} alt={'revenue-icon'} text= {"Revenue"}></BoxItem>
            <BoxItem className={"hide"} src={bell} alt={'bell-icon'} text= {"Notificationsd"}></BoxItem>
            <BoxItem className={"hide"} src={analytics} alt={'analytic-icon'} text= {"Analytics"}></BoxItem>
            <BoxItem className={"hide"} src={inventory} alt={'inventory-icon'} text= {"Inventory"}></BoxItem>
          </nav>
          <div className="side-footer">
          <BoxItem  className={"hide"} src={exit} alt={'exit-icon'} text= {"Logout"}></BoxItem>
            <div className="side-footer-item">
              <div className="hide side-footer-item-img">
                <Icon src={sunLength} alt={'sunLength-icon'}></Icon>
              </div>
              <Paragraph className={"hide item-text"} text={"Light mode"}/>
                <label className='side-footer-btn' htmlFor='toggle'>
                  <input type='checkbox' name='toggle' id='toggle' className="side-footer-btn__input"/>
                  <span className="side-footer-btn__display Toggle__display" hidden>
                  <Icon src={sunYellow} alt={'sun-yellow-icon'}></Icon> 
                  <Icon src={moon} alt={'moon-icon'}></Icon>   
                  </span>
                </label>
            </div>
          </div>
        </div>
      </aside>
    )
}
export default Aside;