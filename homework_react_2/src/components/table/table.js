import React from "react";
import TbodyElement from "../tbody-element/tbody-element";

function Table (){
    return(
        <table>
            <thead>
                <tr>
                <th colSpan="3">Знаки зодіаку по місяцях</th>
                <th>Період</th>
                </tr>
            </thead>
            <tbody>
            <TbodyElement num={"1"} icon={<>&#9800;</>}  name={'Овен'} text={'21 березня-20 квітня'}/>
            <TbodyElement num={"2"} icon={<>&#9801;</>}  name={'Телець'} text={'21 квітня-21 травня'}/>
            <TbodyElement num={"3"} icon={<>&#9802;</>}  name={'Близнята'} text={'22 травня-21 червня'}/>
            <TbodyElement num={"4"} icon={<>&#9803;</>}  name={'Рак'} text={'22 червня-22 липня'}/>
            <TbodyElement num={"5"} icon={<>&#9804;</>}  name={'Лев'} text={'23 липня-23 серпня'}/>
            <TbodyElement num={"6"} icon={<>&#9805;</>}  name={'Діва'} text={'24 серпня-23 вересня'}/>
            <TbodyElement num={"7"} icon={<>&#9806;</>}  name={'Терези'} text={'24 вересня-23 жовтня'}/>
            <TbodyElement num={"8"} icon={<>&#9807;</>}  name={'Скорпіон'} text={'23 жовтня-22 листопада'}/>
            <TbodyElement num={"9"} icon={<>&#9808;</>}  name={'Стрілець'} text={'23 листопада-22 грудня'}/>
            <TbodyElement num={"10"} icon={<>&#9809;</>}  name={'Козеріг'} text={'23 грудня-20 січня'}/>
            <TbodyElement num={"11"} icon={<>&#9810;</>}  name={'Водолій'} text={'21 січня-20 лютого'}/>
            <TbodyElement num={"12"} icon={<>&#9811;</>}  name={'Риби'} text={'21 лютого-20 березня'}/>
            </tbody>
        </table>
    )
}

export default Table;