import Section from "../section";

function Main (){
    return(
        <main className="main-content">
            <Section/>
        </main>
    )
};

export default Main;