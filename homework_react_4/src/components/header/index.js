import logo from "../../image/logo.jpg";

function Header(){
    return(
        <header className="header">
            <nav className="header-nav">
                <a className="header-logo" href="/">
                    <img src={logo} alt="logo"/>
                </a>
                <h1 className="header-title">Домашня робота з 'React' №4 Влад Бродич</h1>
            </nav>
        </header>
    )
}

export default Header;