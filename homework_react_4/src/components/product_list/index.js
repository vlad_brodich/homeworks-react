import {createProductList} from '../../methods/index.js';

function ProductList({data}){
    let list = <li className='error'>На жаль данні від сервера не отриманні.</li>;

    if(Array.isArray(data)){
        list = createProductList(data);
    }

    return(
        <ul className="homework-product-list">
            {
                list
            }
        </ul>
    )
}

export default ProductList;


