import {createBtn} from '../../methods/index.js';

export default function Navigation({products,click}){
    let btns = createBtn(products,click)
    return(
        <nav className="nav-filter">
            {btns}
        </nav>
    )
}