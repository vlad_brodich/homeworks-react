
function Footer(){
    return(
        <footer className="footer">
            <div className="footer-box">
                <a href="mailto:brodich_vlad@ukr.net">brodich_vlad@ukr.net</a>
                <p className="footer-box-text">04 Червня 2023р.</p>
            </div>
        </footer>
    )
}

export default Footer;