import {Component} from "react";
import Loader from "../loader";
import ProductList from "../product_list";
import Navigation from "../navigation";
import {categoryFilter} from '../../methods/index.js'

class Section extends Component{
    state = {
        loaderClass:'loader-box ',
        products:'',
        productsFilter:'',
        navBtn:'',
    }

    // Запит на сервер після побудови сторінки.
    componentDidMount(){
        fetch('https://fakestoreapi.com/products')
        .then(res => res.json())
            .then((result) => {
                // Перевірка чи відбулися зміни після запиту.
                if(this.state.products.length !== result.length){
                    this.setState((state) => {
                        return{
                            ...state,
                            loaderClass:'loader-box hide',
                            products:result,
                        }
                    })
                }

            })
         
    }

    // кнопки фільтра категорій.
    click=(e)=>{
        if(e === 'all' && this.state.navBtn !== e){
            this.setState((state) => {
                return{
                    ...state,
                    productsFilter:this.state.products,
                    navBtn:e,
                }
            })
        }
        else if(this.state.navBtn !== e){
            this.setState((state) => {
                return{
                    ...state,
                    productsFilter:categoryFilter(this.state.products,e),
                    navBtn:e,
                }
            })
        }
        else return
    }


    render(){
        return(
            <section className="homework">
                <h2 className="homework-title">Використовуючи методи життєвого циклу,<br/> AJAX та класові компоненти створіть запит на сервер за адресою https://fakestoreapi.com/products  отримайте продукти та виведіть на сторінку.</h2>
                <Navigation products={this.state.products} click={this.click}/>
                <ProductList data={this.state.productsFilter === ''? this.state.products:this.state.productsFilter}/>
                <Loader className={this.state.loaderClass}/>
            </section>
        )
    }
};

export default Section;