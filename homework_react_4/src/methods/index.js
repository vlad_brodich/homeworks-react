// Функція створення ID або Key.
export function getId(num){
    const ABC = "AaBbCcDdEeFfGgHhIiJiKkLlMmNnJjPpQqRrSsTtUuVvWwXxYyZz";
    const NUMBERS = "0123456789";
    const LETTERS = ABC + NUMBERS;
    let newId= "";
    for (let i = 0; i < 8; i++) {
        newId += LETTERS[Math.floor(Math.random() * LETTERS.length)];
    }
    newId +=  `_${num}`;
    return newId;
}

// Створює зірочки рейтинг.
function ratingStars (rating){
    let x = Math.round(rating);
        const star = (color)=>{return <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M11.8205 0L15.3997 7.25312L23.4039 8.41582L17.6118 14.0612L18.9794 22.0329L11.8205 18.2688L4.66156 22.0329L6.02918 14.0612L0.237098 8.41582L8.24126 7.25312L11.8205 0Z" fill={color}/>
    </svg>};

        let stars =[]
        for (let i = 0; i < 5; i++) {
            if(i < x){
                stars.push(<li key={getId(i)}>{star("#FFCC48")}</li>)
            }
            else {
                stars.push(<li key={getId(i)}>{star("#BCBCCC")}</li>)
            }
        }
        return stars
}

// Створює карточки товарів.
export function createProductList(data){
    const list =  data.map(({id,title,price,description,category,image,rating},i) => {
        return( <li key={getId(i)} className="product-cart">
            <p className="product-cart_id">#{id}</p>
        <div className="product-cart_img">
            <img src={image} alt={title}/>
        </div>
        <p className="product-cart_category">Category:<span> {category}</span></p>
        <h3 className="product-cart_title">{title}</h3>
        <div className="product-cart_description-box">
        <p className="product-cart_description">{description}</p>
        </div>
        
        <p className="product-cart_price">Price: <span>{price} </span>USD</p>
        <ul className="product-cart_rating">
            {ratingStars (rating.rate)}
        </ul>
        <p className="product-cart_count">availability: {rating.count} pcs</p>
        </li>)
    });
    return list;
}

// Функція створення назв кнопок фільтра.
function createBtnName(data){
    const namesBtn = ['all'];

    for(let i = 0; i < data.length; i++){
        if(!namesBtn.includes(data[i].category)){
            namesBtn.push(data[i].category)
            
        }
    }
    return namesBtn
}

// Функція створення кнопок фільтра.
export function createBtn(data,colbak){
    const btnsName = createBtnName(data)
    const btns = btnsName.map((e,i)=>{
       return <button key={getId(i)} className="nav-btn" type="button" onClick={()=>{colbak(e.slice(0,3))}}>{e}</button>
    })
    return btns
}

// Функція фільтр за категорією.
export function categoryFilter(arr,category) {
    return arr.filter((el) => el.category.slice(0,3).includes(category));
  }