import { BrowserRouter } from "react-router-dom";
import { Routes,Route } from "react-router";

import ErrorPage from '../../pages/error-page';
import MainPage from '../../pages/main-page';
import TemplatePage from '../../pages/template-page';
import { req } from "../../methods";
import { useEffect, useState } from "react";
import {creatKey} from '../../methods';
import CategoryPage from "../../pages/category-page";


export default function App(){
    // Змінна ключів обєкта отриманого від сервера.
    const [dataObj, setDataObj] = useState(null);

    useEffect(() => {
        const data = req('https://rickandmortyapi.com/api');
        data.then(function (info) {
            setDataObj(info)
        })
    },[])

    // Функція створення Route відповідно кількості сторінок.
    const creatRout = (data) =>{
        const keys = Object.keys(data)
        const rout = keys.map((e,i)=>{
            return(
                <Route key={creatKey(i)} path={e+'/*'} element={<CategoryPage urlPage={data[e]} title={e}/>}></Route>
            )
        })
        return rout
    }
   
    return(
        <>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<TemplatePage links={dataObj && Object.keys(dataObj)}/>}>
                        <Route index element={<MainPage links={dataObj && Object.keys(dataObj)}/>}></Route>
                            {dataObj &&  creatRout(dataObj) }
                        <Route path="*" element={<ErrorPage/>}></Route>
                    </Route>
                </Routes>
            </BrowserRouter>
        </>
    )
}