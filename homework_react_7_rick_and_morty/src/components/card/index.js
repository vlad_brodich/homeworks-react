import { creatKey } from '../../methods';
import { Link } from 'react-router-dom';

export default function Card({data,clickCard}){
    const arr = data.results.map((el,i) => {
        return (
            <Link to={'details'} key={creatKey(i)}>
            <li onClick={()=>{
                clickCard(el.url)
            }} className='page__list-item'>
                { el.image && <div className='page__list-item__img'>
                    <img src={el.image} alt={el.name} />
                </div>}
                <div className='page__list-item__info'>
                    <h3 className='page__list-item__info-title'>{el.name}</h3>
                    {el.species && <p  className='page__list-item__info-text'>{el.species}</p>}
                    {el.dimension && <p  className='page__list-item__info-text'>{el.dimension}</p>}
                    {!el.image && el.type && <p  className='page__list-item__info-text'>{el.type}</p>}
                    {el.air_date && <p  className='page__list-item__info-text'>{el.air_date}</p>}

                    {el.air_date && el.episode && <p  className='page__list-item__info-text'>{el.episode}</p>}
            
                    {!el.image && el.created && <p  className='page__list-item__info-text'>{el.created}</p>}
                </div>
            </li>
            </Link>
        )
      
    })
    return arr;
}