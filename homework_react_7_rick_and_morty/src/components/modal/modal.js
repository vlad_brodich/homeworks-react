import './modal.css';

export default function Modal({ dataDetails, closeModal }) {

    return (
        <div className='modal' onClick={() => { closeModal() }}>
            <div>
                <h3 className='modal__title'>{dataDetails.name}</h3>
                
                <div className={dataDetails.type?'modal__img locations': 'modal__img episodes'}>{dataDetails.image && <img src={dataDetails.image} alt={dataDetails.name} />}</div>
               

                {dataDetails.type && <p className='modal__text'><span>type: </span>{dataDetails.type}</p>}

                {dataDetails.dimension && <p className='modal__text'><span>dimension: </span>{dataDetails.dimension}</p>}


                {dataDetails.air_date && <p className='modal__text'><span>air date: </span>{dataDetails.air_date}</p>}

                {dataDetails.status && <p className='modal__text'><span>status: </span>{dataDetails.status}</p>}

                {dataDetails.species && <p className='modal__text'><span>species: </span>{dataDetails.species}</p>}

                {dataDetails.gender && <p className='modal__text'><span>gender: </span>{dataDetails.gender}</p>}

                <p className='modal__text'><span>{dataDetails.image ? 'born: ' : 'created: '}</span>{dataDetails.created}</p>

            </div>
        </div> 
    )
}