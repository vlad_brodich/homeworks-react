import { Link } from "react-router-dom";
import { creatKey } from "../../methods";

export default function NavList({data, className, flag = false}){
    const link = data.map((e,i)=>{
        return(
            <li key={creatKey(i)} className={ className }>

                {flag ? <Link to={e}>
                    <div className={"linck-img-" + e}></div>
                    <p>{e}</p>  
                </Link>:<Link to={e}>{e}</Link>}
                
            </li>
        )
    })
    return link
}