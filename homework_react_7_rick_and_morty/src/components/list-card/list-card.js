import './list-card.css';
import Card from '../card';

export default function ListCard({dataList, clickCard}){

    return(
        <ul className='page__list'>
            {dataList && <Card data={dataList} clickCard={clickCard}/>}
        </ul>
    )
}