import { Link } from "react-router-dom";

import "./error-page.css";

export default function ErrorPage(){
    return(
        <div className="error-page">
            <div className="error-page__box">
                <h1 className={'error-page__title'}>'Помилка 404 Нажаль такої сторінки не існує'</h1>
                <Link to={'..'}><div className="error-page__btn" >Повернутися на головну сторінку</div></Link>
            </div>
        
        </div>
    )
}