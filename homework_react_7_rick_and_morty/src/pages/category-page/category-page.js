import { useState, useEffect } from 'react';
import './category-page.css';
import { req } from '../../methods';
import { Route, Routes} from 'react-router';
import ListCard from '../../components/list-card';

import DetailsPage from '../details-page/details-page';
import Loader from '../../components/loader/loader';
import ErrorPage from '../error-page';

    
export default function CategoryPage({urlPage,title}){

    const [ dataPage, setDataPage ] = useState(null);
    const [ urlDetailsPage, setUrlDetailsPage ] = useState(null);

    useEffect(() => { 
            const data = req(urlPage);
            data.then(function (info) {
                setDataPage(info)
            })
    },[urlPage])

    // Клік попередня сторінка.
    const clickPrev = (data) => {
        if(data.prev){
            const d = req(data.prev);
            d.then(function (info) {
                setDataPage(info)
                window.scrollTo(0, 0)
            })
        }
    }
    // Клік наступна сторінка.
    const clickNext = (data) => {
        if(data.next){
            const d = req(data.next);
            d.then(function (info) {
                setDataPage(info)
            })
            window.scrollTo(0, 0)
        }
    }
   // Клік карточку.
    const clickCard = (url) =>{
        setUrlDetailsPage(url)
    }

    return(
        <Routes>
            <Route index element={
                <section className='category-page'>
                   { !dataPage && <Loader/> }
                    <h2 className='category-page__title'>{title}</h2>
                    {dataPage && <ListCard dataList={dataPage} clickCard={clickCard}/>}

                    <div className='category-page__pagination'>
                        {dataPage && dataPage.info.prev && 
                        <button className='pagination-btn' onClick={() => { clickPrev(dataPage.info) }} type='button'>&#10094; Prev</button>}
                        
                        {dataPage && dataPage.info.next && 
                        <button className='pagination-btn' onClick={() => { clickNext(dataPage.info) }} type='button'>next &#10095;</button>}
                    </div>
                </section>}>    
            </Route>

            <Route path='details' element={<>
                <h2 className='category-page__title'>{title} details</h2><DetailsPage url={ urlDetailsPage && urlDetailsPage } namePage={title}/></>}>
            </Route>
            <Route path='*' element={<ErrorPage/>}></Route>
        </Routes>
    )
}