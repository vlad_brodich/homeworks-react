import { Link, Outlet } from 'react-router-dom';
import './template-page.css';
import logo from '../../img/logo-black.svg';
import NavList from '../../components/nav-list';

export default function TemplatePage({links}){

    return(
        <div className='page'>
            <header className='header'>
                <nav className='header__nav'>
                    <Link to={'/'}>
                        <div className='header__nav-logo'>
                            <img src={logo} alt='logo'/>
                        </div>
                    </Link>
                    <ul className='header__nav-list'>
                        {links && <NavList data={links} className={'header__nav-list__item'}/>}
                    </ul>
                </nav>
            </header>
            <main>
                <h1 className='visually-hidden'>rick and mortyapi</h1>
                <Outlet/>
            </main>
            <footer className='footer'>
                <div className='footer__nav'>
                <a className='footer__nav-link'  href="mailto:brodich_vlad@ukr.net">brodich_vlad@ukr.net</a>
                <span className='footer__nav-date'>08.07.2023</span>
                </div>
            </footer>
        </div>
    )
}