import './details-page.css';
import { useState, useEffect } from 'react';
import { req, creatKey } from '../../methods';
import { Link } from 'react-router-dom';
import  Modal  from '../../components/modal';
import Loader from '../../components/loader/loader';

export default function DetailsPage({url,namePage}){
    // Стан данних сторінки.
    const [ dataPage, setDataPage ] = useState(null);
    const [ loader, setLoader] = useState(false);
    useEffect(() => {
        setLoader(true)
        const data = req(url);
        data.then(function (info) {
            setDataPage(info)
            setLoader(false)
        })
    },[url])

    // Стан  модального вікна.
    const [ dataDetails, setDataDetails ] = useState(null);
    // Функція клік докладніше та запит для інфо модальноговікна.
    const showDetails = (urlDetails)=>{
        setLoader(true)
        const data = req(urlDetails);
        data.then(function (info) {
            setDataDetails(info)
            setLoader(false)
        })
    }

// Функція створення шаблону сторінки детальніше.
    const showPage = ({name, image, status, species, type, gender, origin, location, episode, created, characters, residents},namePage) =>{
        return(
            <div className='details-page__info-box'>
                
                <div className={'info-box__img '+ namePage}>{image && <img src={image} alt={name} />}</div>

                <div className='info-box__info'>
                    <h1 className='info-box__title'>{"details - " + name}</h1>

                    {status && <p className='info-box__text'><span>status: </span>   {status}</p>}

                    {species && <p className='info-box__text'><span>species: </span>{species}</p>}

                    {type && type !=='' && <p className='info-box__text'><span>type: </span>{type}</p>}

                    {gender && <p className='info-box__text'><span>gender: </span>{gender}</p>}
                
                    {origin ? origin.url !==''? <p className='info-box__linck' onClick={()=>{showDetails(origin.url)}}><span>origin: </span>{origin.name}</p>:<p className='info-box__text'><span>origin: </span>{origin.name}</p>:null}

                    {location ? location.url !==''?<p className='info-box__linck' onClick={()=>{showDetails(location.url)}}><span>location: </span>{location.name}</p>:<p className='info-box__text'><span>location: </span>{location.name}</p>:null}


                    {episode ? Array.isArray(episode) ? <> <h3 className='info-box__list-title'>episodes</h3><ul className='info-box__list'>{episode.map((e,i)=>{
                    return(<li className='info-box__list-item' key={creatKey(i)}><p className='info-box__linck' onClick={()=>{showDetails(e)}}>{`episode - ${i+1}`}</p></li>)
                    })}</ul></> : <p className='info-box__text'><span>episode: </span>{episode}</p> : null}
                    

                    {characters && Array.isArray(characters) && <> <h3 className='info-box__list-title' >characters</h3><ul className='info-box__list'>{characters.map((e,i)=>{
                    return(<li className='info-box__list-item' key={creatKey(i)}><p className='info-box__linck' onClick={()=>{showDetails(e)}}>{`character - ${i+1}`}</p></li>)
                    })}</ul></>}

                    {residents && Array.isArray(residents)&& residents.length > 0 &&<> <h3 className='info-box__list-title'>residents</h3><ul className='info-box__list'>{residents.map((e,i)=>{
                    return(<li className='info-box__list-item' key={creatKey(i)}><p className='info-box__linck' onClick={()=>{showDetails(e)}}>{`character - ${i+1}`}</p></li>)
                    })}</ul></>}

                    {created && <p className='info-box__text'><span>{image ? 'born: ' : 'created: '} </span>{created}</p>}
                </div>    
            </div>
        )
    }

   
// Клік зачинити модалку.
    const  closeModal = () =>{
        setDataDetails(null)
    }

    return(

        <section className='details-page'>
           { loader && <Loader/>}
            <Link to={'..'} className='details-page_btn-back'>&#10094; back</Link>
            {dataPage && showPage(dataPage,namePage)}

            {dataDetails && <Modal dataDetails={dataDetails} closeModal={closeModal}/>}
        </section>
    )
}