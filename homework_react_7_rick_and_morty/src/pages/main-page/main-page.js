import './main-page.css'
import main_img from '../../img/rick-and-morty.svg'
import NavList from '../../components/nav-list';


export default function MainPage({links}){

    return(
        <div className='main-page'>
            <div className='main-page__img'>
                <img src={main_img} alt='rick-and-morty'/>
            </div>
            <div className='main-page__search'>
                <input className='search-input' type='text' placeholder='Filter by name or episode (ex. S01 or S01E02)'/>
            </div>
            <nav className='main-page__nav'>
                <ul className= 'main-page__nav-list'>
                    {links && <NavList data={links} className={'main-page__nav-list__item'} flag={true}/>}
                </ul>
            </nav>
        </div>
    )
}