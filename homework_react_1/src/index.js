import React from "react";
import ReactDOM  from "react-dom";

function Table() {
    return (
        <table>
            <thead>
                <tr>
                <th colspan="3">Знаки зодіаку по місяцях</th>
                <th>Період</th>
                </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>&#9800;</td>
                <td>Овен</td>
                <td>21 березня-20 квітня</td>
            </tr>
            <tr>
                <td>2</td>
                <td>&#9801;</td>
                <td>Телець</td>
                <td>21 квітня-21 травня</td>
            </tr> 
            <tr>
                <td>3</td>
                <td>&#9802;</td>
                <td>Близнята</td>
                <td>22 травня-21 червня</td>
            </tr> 
            <tr>
                <td>4</td>
                <td>&#9803;</td>
                <td>Рак</td>
                <td>22 червня-22 липня</td>
            </tr> 
            <tr>
                <td>5</td>
                <td>&#9804;</td>
                <td>Лев</td>
                <td>23 липня-23 серпня</td>
            </tr> 
            <tr>
                <td>6</td>
                <td>&#9805;</td>
                <td>Діва</td>
                <td>24 серпня-23 вересня</td>
            </tr> 
            <tr>
                <td>7</td>
                <td>&#9806;</td>
                <td>Терези</td>
                <td>24 вересня-23 жовтня</td>
            </tr> 
            <tr>
                <td>8</td>
                <td>&#9807;</td>
                <td>Скорпіон</td>
                <td>23 жовтня-22 листопада</td>
            </tr> 
            <tr>
                <td>9</td>
                <td>&#9808;</td>
                <td>Стрілець</td>
                <td>23 листопада-22 грудня</td>
            </tr> 
            <tr>
                <td>10</td>
                <td>&#9809;</td>
                <td>Козеріг</td>
                <td>23 грудня-20 січня</td>
            </tr> 
            <tr>
                <td>11</td>
                <td>&#9810;</td>
                <td>Водолій</td>
                <td>21 січня-20 лютого</td>
            </tr> 
            <tr>
                <td>12</td>
                <td>&#9811;</td>
                <td>Риби</td>
                <td>21 лютого-20 березня</td>
            </tr> 
            </tbody>
        </table>
    )
}

ReactDOM.render(<Table></Table>, document.querySelector('#result'));